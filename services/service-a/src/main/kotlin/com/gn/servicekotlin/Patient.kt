package com.gn.servicekotlin

/**
 * @author caobingsheng
 * @date 2021/5/27 17:27
 * @version 1.0
 */
data class Patient(val id: Int, val name: String)
