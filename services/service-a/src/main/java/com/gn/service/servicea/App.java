package com.gn.service.servicea;

import com.gn.servicekotlin.Patient;
import com.gn.suba.User;

/**
 * @author caobingsheng
 * @version 1.0
 * @date 2021/5/27 17:12
 */
public class App {
    public static void main(String[] args) {
        Patient patient = new Patient(10, "李四");
        System.out.println("service AAAAA" + patient);
        User user = new User();
        user.setId(10);
        user.setName("张三");

        System.out.println("user:" + user);
    }
}
