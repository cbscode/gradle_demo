# gradle 基本使用

1. task-hello
    - 运行 `gradle -q hello` 或 `gradle hello`

2. task-demo
    - 运行 `gradle -q demo` 或 `gradle demo`

3. task-upper
    - 运行 `gradle -q upper` 或 `gradle upper`

4. task-useTimes
    - 运行 `gradle -q useTimes` 或 `gradle useTimes`

4. task-depend
    - 运行 `gradle -q depend` 或 `gradle depend`

5. task-0:4
    - 运行 `gradle -q task0` 或 `gradle task0`

6. task-dynamicDepend
    - 运行 `gradle -q task0` 或 `gradle task0`

7. task-named
    - 运行 `gradle -q target` 或 `gradle target`

8. task-ant
    - 运行 `gradle -q loadfile` 或 `gradle loadfile`

9. task-useMethod
    - 运行 `gradle -q useMethod` 或 `gradle useMethod`

10. task-defaultTasks
    - 运行 `gradle -q` 或 `gradle`

11. task-encode
    - 运行 `gradle -q encode` 或 `gradle encode`

12. task-configure
   - 运行 `gradle -q taskA` 或 `gradle taskA`

13. task-findAll
- 运行 `gradle -q taskx` 或 `gradle taskx`

14. task-dependsOnOtherProject
- 运行 `gradle -q pa_taskA` 或 `gradle pa_taskA`

15. task-onlyIf
- 运行 `gradle -q skiped` 或 `gradle skiped`

16. task-stop
- 运行 `gradle -q callStop` 或 `gradle callStop`

16. task-enabled
- 运行 `gradle -q disabled` 或 `gradle disabled`

17. task-timeout
- 运行 `gradle -q timeout` 或 `gradle timeout`

18. task-addRule
- 运行 `gradle -q groupPing` 或 `gradle groupPing`

19. task-finally
- 运行 `gradle -q begin` 或 `gradle begin`

20. task-ext
- 运行 `gradle -q print_version` 或 `gradle print_version`

21. task-build
- 运行 `gradle -q build` 或 `gradle build`

22. task-copy
- 运行 `gradle -q copyPdf` 或 `gradle copyPdf`

23. task-run
- 运行 `gradle -q run` 或 `gradle run`  或 `gradle -q :sub_project_a:run`  或 `gradle :sub_project_a:run`

24. task-subproject
- 运行 `gradle -q :services:service-a:run`

25. task-kotlin
- 运行 `gradle -q :services:service-a:run`

26. task-callOtherProject
- 运行 `gradle -q :services:service-a:run`

27. dynamicIncludeModule

28. autoScanSubModules
